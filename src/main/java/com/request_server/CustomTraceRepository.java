package com.request_server;

import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Repository
public class CustomTraceRepository implements HttpTraceRepository {

    AtomicReference<List<HttpTrace>> traces = new AtomicReference<>();

    @Override
    public List<HttpTrace> findAll() {
        return traces.get();
    }

    @Override
    public void add(HttpTrace trace) {
        if ("GET".equals(trace.getRequest().getMethod())) {
           List<HttpTrace> tracesList = new ArrayList<>();
           if (traces.get() != null) {
               for (int i = 0; i < traces.get().size(); i++) {
                   tracesList.add(traces.get().get(i));
               }
           }
           tracesList.add(trace);
           traces.set(tracesList);
        }
    }

}
