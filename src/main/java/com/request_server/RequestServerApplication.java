package com.request_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RequestServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RequestServerApplication.class, args);
	}

}
