package com.request_server;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.apache.tomcat.util.json.ParseException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@RestController
public class RequestController {
    @RequestMapping(value = "/**", method = {RequestMethod.GET, RequestMethod.POST})
    public void handle(HttpServletRequest request, @RequestParam Map<String,String> allRequestParams) throws IOException, ParseException {

    }
}

